using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerMovement : CharacterMovement
{
    [SerializeField] private float maxSpeed = 10f;

    [SerializeField] private float jumpForce = 5f;

    private Rigidbody rigidbody;

    private void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        InputManager.JumpAction += OnJump;
    }

    private void OnDestroy()
    {
        InputManager.JumpAction -= OnJump;
    }

    private void FixedUpdate()
    {
        if (IsFrizing)
        {
            Vector3 velocity = rigidbody.velocity;
            velocity.x = 0f;
            velocity.z = 0f;
            rigidbody.velocity = velocity;
        }
        
        Vector3 direction = new Vector3(InputManager.HorizontalAxis, 0f, InputManager.VerticalAxis);
        
        if (!IsGrounded())
        {
            direction *= 0.5f;
        }
        
        Move(direction);
    }

    private bool IsGrounded()
    {
        Vector3 point = transform.position;
        point.y -= 0.9f;
        RaycastHit hit;
        Ray ray = new Ray(point, point + Vector3.down);

        Debug.Log("IsGrounded(): " + Physics.Raycast(ray,  out hit, 1f));
        return Physics.Raycast(ray,  out hit, 1f);
    }

    public override void Move(Vector3 direction)
    {
        Vector3 velocity = rigidbody.velocity;
        velocity.x = direction.x * maxSpeed;
        velocity.z = direction.z * maxSpeed;
        rigidbody.velocity = velocity;
    }

    public override void Stop(float time)
    {
        
    }

    public override void Jump(float force)
    {
        rigidbody.AddForce(new Vector3(0f, force, 0f), ForceMode.Impulse);
    }

    private void OnJump(float inputForce)
    {
        if (IsGrounded())
        {
            Jump(inputForce * jumpForce);
        }
    }
}
