using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour, IPlayer, IHitBox
{
    [SerializeField] 
    private float m_Health;

    public float Health
    {
        get => m_Health;
        private set
        {
            m_Health = value;
            if (m_Health <= 0)
            {
                Die();
            }
        }
    }

    [SerializeField] 
    private Camera camera;

    public void Die()
    {
        Debug.Log("Player is dead :C");
    }

    public void Hit(float hit)
    {
        m_Health -= hit;
    }

    public void RegisterPlayer()
    {
        // Do something
    }

    private void Start()
    {
        InputManager.HitAction += OnAttack;
    }

    private void OnDisable()
    {
        InputManager.HitAction -= OnAttack;
    }

    private void OnAttack(string axis)
    {
        Debug.Log("Get Attack!");
    }
}
