using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public static float HorizontalAxis;
    public static float VerticalAxis;

    public static event Action<float> JumpAction;
    public static event Action<string> HitAction;

    public float JumpForce = 1.25f;

    private float jumpTimer;
    private Coroutine waitJumpCoroutine;

    private void Start()
    {
        HorizontalAxis = 0;
        VerticalAxis = 0;
    }

    private void Update()
    {
        HorizontalAxis = Input.GetAxis("Horizontal");
        VerticalAxis = Input.GetAxis("Vertical");
        
        if (Input.GetButtonDown("Jump"))
        {
            JumpAction.Invoke(JumpForce);
        }

        if (Input.GetKey(KeyCode.LeftShift))
        {
            HorizontalAxis += HorizontalAxis > 0 ? 0.5f : -0.5f;
            VerticalAxis += VerticalAxis > 0 ? 0.5f : -0.5f;
        }

        if (Input.GetButtonDown("Fire1"))
        {
            HitAction?.Invoke("Fire1");
        }
    }
}
