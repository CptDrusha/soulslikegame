using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlayer
{
    void RegisterPlayer();
}

public interface IEnemy
{
    void RegisterEnemy();
}

public interface IDamage
{
    int Damage { get; }
    void SetDamage();
}

public interface IHitBox
{
    float Health { get; }
    void Hit(float damage);
    void Die();
}
